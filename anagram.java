package Anagram;

import java.io.*;
import java.util.*;

/**
 *
 * @author Allan Hawkins
 */
public class anagram {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        String fileName1; // Holds input file path
        String fileName2 = "output.txt"; // Holds output file path
        String line; // Holds line read
        String strFile;
        String strFileLowercase;
        String strModified;


        // Creates scanner object 
        Scanner keyboard = new Scanner(System.in);

        // Prints out message to user 
        System.out.println("Enter name of file to be read: ");
        fileName1 = keyboard.nextLine(); // Stores user data
        // Precondition: The calling code passes no parameter 
        // Postcondition: assigns string to variable

        // Creates FileReader object that opens input file
        FileReader freader = new FileReader(fileName1);
        // Precondition: The calling code provides a string 
        // Postcondition: opens file

        // Creates object to store data read from input file
        BufferedReader inputFile = new BufferedReader(freader);
        // Precondition: The calling code provides a string 
        // Postcondition: establishes a connection with the opened file

        // Creates object that opens output file
        FileWriter fwriter = new FileWriter(fileName2);
        // Precondition: The calling code provides a string 
        // Postcondition: opens file

        // Creates object that can print to output file
        PrintWriter outputFile = new PrintWriter(fwriter);
        // Precondition: The calling code provides a string 
        // Postcondition: establishes a connection with the opened file

        // Reads in line form the input file and stores it in line
        line = inputFile.readLine();
        // Precondition: The calling code passed no parameter 
        // Postcondition: assigns string to variable

        // Test condition if input file is empty
        if (line == null) {
            System.out.println("Input file is empty");
            // Precondition: The calling code provides a string 
            // Postcondition: displays string on screen
            System.exit(0);
        }

        //Creates an object that enbles us to concatenate string objects 
        StringBuilder file = new StringBuilder();
        // Precondition: The calling code passed no parameter 
        // Postcondition: reference varible is created

        //Loop that perfoems the concatenation of string objects
        while (line != null) {
            file.append(" ");
            // Precondition: The calling code passes a space character
            // Postcondition: Concatenates a space to the string object 
            file.append(line);
            // Precondition: The calling code provides a string object
            // Postcondition: Concatenates string object
            line = inputFile.readLine(); 
            // Precondition: The calling code passed no parameter 
            // Postcondition: assigns string to variable
        }

        //Converts string object to string
        strFile = file.toString();
        // Precondition: The calling code passed no parameter 
        // Postcondition: assigns string to variable

        //Tokenizes the string 
        StringTokenizer tokens = new StringTokenizer(strFile);
        // Precondition: The calling code passed a string 
        // Postcondition: create tokens of strings 

        //Creates string arraylist
        ArrayList<String> words = new ArrayList<String>();
        // Precondition: The calling code passed no parameter
        // Postcondition: variable is assigned a reference to the arraylist

        //Loop that adds the tokens to the words arraylist
        while (tokens.hasMoreTokens()) {
            words.add(tokens.nextToken());
        }
        
        //Converts string to lower case
        strFileLowercase = strFile.toLowerCase();
        // Precondition: The calling code passed no parameter
        // Postcondition: assigns string to variable
        
        //Stripes string of non alphabetical characters
        strModified = strFileLowercase.replaceAll("[^a-z\\sA-Z]", "");
        // Precondition: The calling code passes regex
        // Postcondition: assigns string to variable
        
        //Creates string arraylist
        ArrayList<String> signature = new ArrayList<String>();
        // Precondition: The calling code passed no parameter
        // Postcondition: variable is assigned a reference to the arraylist
        
        //Tokenizes the string
        StringTokenizer tokens2 = new StringTokenizer(strModified);
        // Precondition: The calling code passed a string 
        // Postcondition: create tokens of strings 
        
        //Loop that sorts the tokens in alphabetical order and addes them 
        //to the signature arraylist
        while (tokens2.hasMoreTokens()) {
            String tempSign = tokens2.nextToken();

            char[] charArr = tempSign.toCharArray();
            Arrays.sort(charArr);
            String r = new String(charArr);
            signature.add(r);

        }

        //Loop that sorts the words and signature arraylist  
        for (int i = 0; i < signature.size(); i++) {
                // Methods retrieve value from an index
                // Precondition: The calling code provides an integer 
                // Postcondition: returns value in index
            for (int j = i + 1; j < signature.size(); j++) {
                if (signature.get(i).compareTo(signature.get(j)) > 0) {

                    String temp = signature.get(i);
                    signature.set(i, signature.get(j));
                    signature.set(j, temp);
                    temp = words.get(i);
                    words.set(i, words.get(j));
                    words.set(j, temp);
                }
            }
        }



        //Loop and if statements that display the anagrams and print to output
        for (int z = 0; z < signature.size() - 1; z++) {

                // Methods that display results 
                //on screen and print to output file
                // Precondition: The calling code provides a string 
                // Postcondition: displays or prints string  
            if (signature.get(z).equals(signature.get(z + 1))) {
                if (signature.get(z + 1).equals(signature.get(z + 2))) {
                    System.out.print(words.get(z));
                    System.out.print(" ");
                    outputFile.print(words.get(z));
                    outputFile.print(" ");
                    System.out.print(words.get(z + 1));
                    System.out.print(" ");
                    outputFile.print(words.get(z + 1));
                    outputFile.print(" ");
                    System.out.print(words.get(z + 2));
                    System.out.print(" ");
                    outputFile.print(words.get(z + 2));
                    outputFile.print(" ");
                    z += 2;
                } else {
                    System.out.print(words.get(z));
                    System.out.print(" ");
                    outputFile.print(words.get(z));
                    outputFile.print(" ");
                    System.out.print(words.get(z + 1));
                    System.out.print(" ");
                    outputFile.print(words.get(z + 1));
                    outputFile.print(" ");
                    z++;
                }
            } else {
                if (z < signature.size() - 2) {
                    System.out.print(words.get(z));
                    System.out.print(" ");
                    outputFile.print(words.get(z));
                    outputFile.print(" ");
                } else {
                    System.out.print(words.get(z));
                    System.out.println();
                    System.out.print(words.get(z + 1));
                    outputFile.print(words.get(z));
                    outputFile.println();
                    outputFile.print(words.get(z + 1));

                }
            }
            System.out.println();
            outputFile.println();
        }
        // Output and input file are closd
        // Precondition: The calling code passes no parameter 
        // Postcondition: close file
        outputFile.close();
        inputFile.close();
    }
}
