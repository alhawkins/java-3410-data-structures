/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarytree;

import java.io.*;

public class Tree<E>
        implements Serializable {

    //class for creating nodes
    protected static class Node< E>
            implements Serializable {
        
        //Declared objects
        protected E data;
        protected Node< E> left;
        protected Node< E> right;

        //method that creates nodes
        public Node(E data) {
            this.data = data;
            left = null;
            right = null;
        }

        //method that creates a string of the node data
        public String toString() {
            return data.toString();
        }
    }
    
    //Declared node object
    protected Node< E> root;

    //method that initializes node root
    public Tree() {
        root = null;
    }

    //method that initializes node object
    protected Tree(Node< E> root) {
        this.root = root;
    }

    
    public Tree(E data, Tree< E> leftTree,
            Tree< E> rightTree) {
        root = new Node< E>(data);
        if (leftTree != null) {
            root.left = leftTree.root;
        } else {
            root.left = null;
        }
        if (rightTree != null) {
            root.right = rightTree.root;
        } else {
            root.right = null;
        }
    }

    public Tree< E> getLeftSubtree() {
        if (root != null && root.left != null) {
            return new Tree< E>(root.left);
        } else {
            return null;
        }
    }

    public Tree<E> getRightSubtree() {
        if (root != null && root.right != null) {
            return new Tree<E>(root.right);
        } else {
            return null;
        }
    }

    public boolean isLeaf() {
        return (root.left == null && root.right == null);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        preOrderTraverse(root, 1, sb);
        return sb.toString();
    }

    private void preOrderTraverse(Node< E> node, int depth,
            StringBuilder sb) {
        for (int i = 1; i < depth; i++) {
            sb.append("  ");
        }
        if (node == null) {
            sb.append("null\n");
        } else {
            sb.append(node.toString());
            sb.append("\n");
            preOrderTraverse(node.left, depth + 1, sb);
            preOrderTraverse(node.right, depth + 1, sb);
        }
    }

    public static Tree< String> readBinaryTree(BufferedReader bR) throws IOException {
        String data = bR.readLine().trim();
        if (data.equals("null")) {
            return null;
        } else {
            Tree< String> leftTree = readBinaryTree(bR);
            Tree< String> rightTree = readBinaryTree(bR);
            return new Tree< String>(data, leftTree, rightTree);
        }
    }
}
