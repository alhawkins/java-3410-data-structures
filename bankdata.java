/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BankData;

import java.util.*;

/**
 *
 * @author allanhawkins
 */
public class bankdata {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Holds current record data 
        String current = "A Program to keep a maintain a bank database:";
        String f_name;//Holds user data
        String l_name;//Holds user data
        String phone;//Holds user data
        int balance;//Holds user data
        int option;//Holds user data
        List<customerInfo> customer = new LinkedList<customerInfo>();
        
        // Creates scanner object 
        Scanner keyboard = new Scanner(System.in);
    
    //Loop that displays menu util asked to quit
    boolean done = false;    
    do{ 
        System.out.println();
        //Displays current record data
        System.out.println("Current record is: " + current);
        //Displays menu
        menu();
        System.out.println("Enter a command from the list above, 1-10: ");
        //passes user option to variable
        option = keyboard.nextInt();
        keyboard.nextLine();
        //Switch that accepts user option and executes a statement
        //depending on option chosen
        switch(option){
            //All option will pass No current record if list in empty    
            //If 1 is chosen, all data will be displyed
            case 1: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else {
                        System.out.println("First Name                Last Name                 Balance      Phone Number");
                        System.out.println("----------                ---------                 -------      ------------");
                        for(int i=0; i < customer.size(); i++){
                        
                            System.out.println(customer.get(i).getF_name() +"          "+ customer.get(i).getL_name()+"           "+customer.get(i).balance()+"           "+customer.get(i).getPhone());
                        }
                    }
            //Will passes a string to current variable if list not empty
            case 2: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else{
                        current = "Current record deleted";
                    }
            //Will change first name in current list data 
            case 3: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else{
                        String newName;//Holds user data
                        //Tokenizes the string 
                        StringTokenizer tokens = new StringTokenizer(current);
                        // Precondition: The calling code passed a string 
                        // Postcondition: create tokens of strings
                    
                        //Creates string arraylist
                        ArrayList<String> currentTokens = new ArrayList<String>();
                        // Precondition: The calling code passed no parameter
                        // Postcondition: variable is assigned a reference to the arraylist

                        //Loop that adds the tokens to the arraylist
                        while (tokens.hasMoreTokens()) {
                            currentTokens.add(tokens.nextToken());
                        }
                        //Changes and passes new data to current variable
                        System.out.println("Enter new name: ");
                        newName = keyboard.nextLine();
                        currentTokens.set(0,newName); 
                        current = currentTokens.get(0)+" "+currentTokens.get(1)+" "+currentTokens.get(2);
                        break;
                        }
            //Will change last name in current list data
            case 4: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else{
                        String newLastName;//Holds user data
                        //Tokenizes the string 
                        StringTokenizer tokens = new StringTokenizer(current);
                        // Precondition: The calling code passed a string 
                        // Postcondition: create tokens of strings
                    
                        //Creates string arraylist
                        ArrayList<String> currentTokens = new ArrayList<String>();
                        // Precondition: The calling code passed no parameter
                        // Postcondition: variable is assigned a reference to the arraylist

                        //Loop that adds the tokens to the arraylist
                        while (tokens.hasMoreTokens()) {
                            currentTokens.add(tokens.nextToken());
                        }
                        //Changes and passes new data to current variable
                        System.out.println("Enter new last name: ");
                        newLastName = keyboard.nextLine();
                        currentTokens.set(1,newLastName); 
                        current = currentTokens.get(0)+" "+currentTokens.get(1)+" "+currentTokens.get(2);
                        break;
                        }
            //Adds new user
            case 5: 
                    //Asks for new customer data
                    System.out.println("Enter First Name: ");
                    f_name = keyboard.nextLine();
                    System.out.println("Enter Last Name: ");
                    l_name = keyboard.nextLine();
                    System.out.println("Enter Phone Number: ");
                    phone = keyboard.nextLine();
                    System.out.println("Enter Balance: ");
                    balance = keyboard.nextInt();
                    //Create node if list is empty, with new customer data
                    if(customer.isEmpty()){
                    customer.add( new customerInfo(f_name, l_name, phone, balance));
                    current = customer.get(0).getF_name() +" "+ customer.get(0).getL_name()+" "+customer.get(0).getPhone();
                    break;
                    }
                    else{
                    //If not empty, create and adds new node in a sorted alphabetically    
                    customerInfo cust = new customerInfo(f_name, l_name, phone, balance);
                            boolean find=false;
                            int index;
                            for(index=0; index < customer.size(); index++)
                            {
                                if(cust.getL_name().compareTo(customer.get(index).getL_name()) < 0){
                                    customer.add(index, cust);
                                    find=true;
                                    current = customer.get(index).getF_name() +" "+ customer.get(index).getL_name()+" "+customer.get(index).getPhone();
                                    break;
                                }
                                if(cust.getL_name().compareTo(customer.get(index).getL_name()) == 0){
                                    if(cust.getF_name().compareTo(customer.get(index).getF_name()) < 0){
                                    customer.add(index, cust);
                                    find=true;
                                    current = customer.get(index).getF_name() +" "+ customer.get(index).getL_name()+" "+customer.get(index).getPhone();
                                    break;                            
                                }
                                    else if(cust.getF_name().compareTo(customer.get(index).getF_name()) == 0){
                                        if(cust.getPhone().compareTo(customer.get(index).getPhone()) == 0){
                                            System.out.println("Customer already exsists");
                                            break;
                                        }
                                        else if(cust.getPhone().compareTo(customer.get(index).getPhone()) < 0){
                                            customer.add(index, cust);
                                            find =true;
                                            current = customer.get(index).getF_name() +" "+ customer.get(index).getL_name()+" "+customer.get(index).getPhone();
                                        break;
                                        }
                                    }
                                }
                                
                            }
                            if(find == false){
                                customer.add(cust);
                                current = customer.get(index).getF_name() +" "+ customer.get(index).getL_name()+" "+customer.get(index).getPhone();
                            }
                                
                             
                       break;     
                            
                    }
                
            case 6: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else{
                        String newPhone;
                        //Tokenizes the string 
                        StringTokenizer tokens = new StringTokenizer(current);
                        // Precondition: The calling code passed a string 
                        // Postcondition: create tokens of strings
                    
                        //Creates string arraylist
                        ArrayList<String> currentTokens = new ArrayList<String>();
                        // Precondition: The calling code passed no parameter
                        // Postcondition: variable is assigned a reference to the arraylist

                        //Loop that adds the tokens to the arraylist
                        while (tokens.hasMoreTokens()) {
                            currentTokens.add(tokens.nextToken());
                        }
                        //Changes and passes new data to current variable
                        System.out.println("Enter new last name: ");
                        newPhone = keyboard.nextLine();
                        currentTokens.set(2,newPhone); 
                        current = currentTokens.get(0)+" "+currentTokens.get(1)+" "+currentTokens.get(2);
                        break;
                        }
            case 7: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
            case 8: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
            //Changes current variable data to a different customer if found
            //in Linkedlist
            case 9: if(customer.isEmpty()){
                       current = "No current record";
                       break;
                    }
                    else{
                 
                    System.out.println("Enter First Name: ");
                    f_name = keyboard.nextLine();
                    System.out.println("Enter Last Name: ");
                    l_name = keyboard.nextLine();
                    System.out.println("Enter Phone Number: ");
                    phone = keyboard.nextLine();
                    
                    boolean find=false;
                    int index;
                    for(index=0; index < customer.size(); index++){
                        if(l_name.compareTo(customer.get(index).getL_name()) == 0 && f_name.compareTo(customer.get(index).getF_name()) == 0 && phone.compareTo(customer.get(index).getPhone()) == 0 ){
                            current = f_name +" "+l_name+" "+phone+" ";
                        }
                    }
                    if(find == false){
                        System.out.println("Person does not exsist");
                        break;
                    }
                    }
            case 10: 
                        done = true;
                        break;
                    
                
        }
    }while(!done);
        
         
        
        
    
    }
        //Method that displays menu
        public static void menu() {
          
          System.out.println();
          System.out.println("1    Show all records");
          System.out.println("2    Delete the current record");
          System.out.println("3    Change the first name in the current record");
          System.out.println("4    Change the last name in the current record");
          System.out.println("5    Add a new record");
          System.out.println("6    Change the phone number in the current record");
          System.out.println("7    Add a deposit to the current balance in the current record");
          System.out.println("8    Make a withdrawal from the current record if there is sufficient funds are available");
          System.out.println("9    Select a record from the record list to become the current record");
          System.out.println("10   Quit");
          System.out.println();
          
        }
}

