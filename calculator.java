/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package Calculator;

import java.io.IOException;
import java.util.*;

/**
 *
 * @author allanhawkins
 */
public class calculator {

    public static void main(String[] args)
            throws IOException {
        String input;// Variable to hold user input 
        Scanner keyboard = new Scanner(System.in);// Creates scanner object
        
        // Prints out message to user 
        System.out.println("Enter infix expression: ");
        input = keyboard.nextLine();// Stores user data
        // Precondition: The calling code passes no parameter 
        // Postcondition: assigns string to variable
        
        String output;// Holds postfix notation
        int result; // Holds sum of postfix 
        calculator calc = new calculator(input);// Create calculator object
        output = calc.doPostfix();// passes data to variable
        System.out.println();// Prints out a blank line
        // Prints out message to user 
        System.out.println("Converted expression: " + output + '\n');
        //result = calc.calculate(output);
        //System.out.println("Answer: " + result + '\n');
    }
    private myStack data;//Creates Stack varaible 
    private String input;//Creates String varible to hold user data  
    private String output = "";//Creates String varible to hold postfix
    private int result; //Creates int varible to hold postfix sum
    private boolean unary = false;//Creates boolean varible
    private int counter = 0;//Creates int varible
    //Creates Stack varaible
    private Stack<String> sum = new Stack<String>();
    
    //Constructor
    public calculator(String in) {
        input = in;
        int stackSize = input.length();
        data = new myStack(stackSize);
    }
    //Method that converts infix to postfix
    // Precondition: The calling code passes no parameter 
    // Postcondition: assigns string to variable
    public String doPostfix() {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            switch (c) {
                case '+':
                case '-':
                    output += " ";                   
                    counter++;
                    if(counter == 2){unary = true;}
                    gotOperator(c);
                    break;
                case '*':
                case '/':
                case '%':
                    output += " ";
                    counter++;
                    if(counter == 2){unary = true;}
                    gotOperator(c);
                    break;
                case '(':
                    data.push(c);
                    counter++;
                    if(counter == 2){unary = true;}
                    break;
                case ')':
                    gotParen(c);
                    break;
                default:
                    counter = 0;
                    output = output + c;
                    break;
            }
        }

        while (!data.isEmpty()) {
            output = output + data.pop();
        }

        return output;
    }

    //Method that handles oprators in input string
    // Precondition: The calling code passes char varible 
    // Postcondition: It stores or removes operators from stack
    public void gotOperator(char opThis) {

        if (data.isEmpty()) {

            if(opThis == '+' && unary){
                unary = false;
                return;
            }
            else if(opThis == '-' && unary){
                data.push('_');
                unary = false;
            }
            else{
            data.push(opThis);
            }
        }

        else {

            char opTop = data.peek();

            if (opThis == '-' || opThis == '+') {

                if (unary) {
                    if (opThis == '+') {
                        unary = false;
                    } else {
                        if (opThis == '-') {
                            data.push('_');
                            unary = false;
                            return;
                        }
                    }

                } else {
                    if (opTop != '(') {

                        output += data.pop();
                    }
                }
            } 
            else if (opThis == '*' || opThis == '/' || opThis == '%') {


                if (opTop != '-' && opTop != '+') {

                    output += data.pop();

                } else {
                    data.push(opThis);
                    return;
                }
            }

            data.push(opThis);
        }
    }
    
    //Method that solves postfix notation
    // Precondition: The calling code passes no parameter 
    // Postcondition: assigns int to variable
    
    /**
    public int doCalc(){
        int calc = 0;
        String temp ="";
        
            
        for (int i = 0; i < output.length(); i++) {
            char c = output.charAt(i);
            switch (c) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':              
                case '6':
                case '7':
                case '8':
                case '9':
                temp += c;
                case ' ': 
                    sum.push(temp);
                    break;
                case '+':
                
                case '-':
                    
                case '*':
                case '/':
                case '%':    
            }
        
        return calc;
    }
    }
    */
    
    //Method that handles oprators in input string
    // Precondition: The calling code passes char varible 
    // Postcondition: Empty Stack
    public void gotParen(char ch) {
        while (!data.isEmpty() && data.peek() != '(') {

            output = output + data.pop();
        }
        data.pop();
    }

    //Stack Class
    class myStack {

        private int maxSize;
        private char[] stackArray;
        private int top;

        public myStack(int max) {
            maxSize = max;
            stackArray = new char[maxSize];
            top = -1;
        }

        public void push(char j) {
            stackArray[++top] = j;
        }

        public char pop() {
            return stackArray[top--];
        }

        public char peek() {
            return stackArray[top];
        }

        public boolean isEmpty() {
            return (top == -1);
        }
    }
}
