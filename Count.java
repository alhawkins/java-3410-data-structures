
package count;
import java.util.*;
/**
 *
 * @author Allan Hawkins
 */
public class Count {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Vector vec = new Vector(10,10);//Creats vector
        int[] arr = new int[100];//Creats Array
        
        System.out.println("10,000 Randomly generated numbers: \n");
        
        //Creats object needed for genereting random integers
        Random randNumber = new Random();
        
        /**
         * Loop that counts the occurrences of integers ranging from 1-99
         * and populates said integers in a vector
         */ 
        for(int i=1; i<10000; i++){
            int k = randNumber.nextInt(99)+1;
            for(int j=1;j<100;j++){
                if(k==j){
                    arr[j]=arr[j]+1;
                }
            }
            vec.add(new Integer (k));
        }
        //Method that sorts vector in ascending order
        Collections.sort(vec);
        
        //Loop that dispalys vector
        for(int j=0; j<vec.size();++j){
            System.out.println("Randomly generated number: "+vec.get(j));
        }    
            System.out.println();
       //Loop that displays occurrences of integers
        for(int j=1; j<100;++j){
            System.out.println("Contains "+arr[j]+" occurences of "+j+"'s");
        }    
          
    }
    
}   

    