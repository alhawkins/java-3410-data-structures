/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BankData;

/**
 *
 * @author allanhawkins
 */
public class customerInfo {
    
    private String f_name;
    private String l_name;
    private String phone;
    private int balance;
    
    public customerInfo(String f, String l, String p, int b){
        
        f_name = f;
        l_name = l;
        phone = p;
        balance = b;
    }
    
    public String getF_name(){
        return f_name;
    }
    
    public String getL_name(){
        return l_name;
    }

    public String getPhone(){
        return phone;
    }
    
    public int balance(){
        return balance;
    }
    
    
}

