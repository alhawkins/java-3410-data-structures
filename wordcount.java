import java.util.*;
import java.io.*;
/**
 *
 * @author allanhawkins
 */
public class wordcount {
    
    public static void main(String[] args)throws IOException {
        
    
                String fileName; // Holds input file path
                // Holds output file path
                String fileName2="output.txt"; 
		String line; // Holds a line read from the input file
		int lc=0; // Line counter
                char c; // Holds character read from line
                // punctuation counters
                int pp=0, ex=0, qq=0, cc=0, cl=0, scl=0, sq=0, dq=0;
                int an=0; // alphanumeric counter
                int vv=0; // vowel counter
                int wc=0; // word counter
                
                // Creates scanner object 
		Scanner keyboard = new Scanner(System.in);
		
                // Prints out message to user 
		System.out.println("Enter name of file to be read: ");
		fileName = keyboard.nextLine(); // Stores us data
                // Precondition: The calling code passes no parameter 
                // Postcondition: assigns string to variable
                
                // Creates FileReader object that opens input file
		FileReader freader = new FileReader(fileName);
                // Precondition: The calling code provides a string 
                // Postcondition: opens file
                
                // Creates object to store data read from input file
		BufferedReader inputFile = new BufferedReader(freader);
		// Precondition: The calling code provides a string 
                // Postcondition: establishes a connection with the opened file
                
                // Creates object that opens output file
		FileWriter fwriter = new FileWriter(fileName2);
                // Precondition: The calling code provides a string 
                // Postcondition: opens file
                
                // Creates object that can print to output file
		PrintWriter outputFile = new PrintWriter(fwriter);
                // Precondition: The calling code provides a string 
                // Postcondition: establishes a connection with the opened file
                
                // Reads in line form the input file and stores it in line
                line = inputFile.readLine();
                // Precondition: The calling code passed no parameter 
                // Postcondition: assigns string to variable
                
                // Test condition if input file is empty
                if(line == null){
                    System.out.println("Input file is empty");
                    // Precondition: The calling code provides a string 
                    // Postcondition: displays string on screen
                }
                
                // Loop that goes through the entire file
                while (line != null)
		{		
			lc++;
                        wc++;
			
                        // Loop that goes through each character in a line
                        for (int j = 0; j < line.length(); j++) {
                             c= line.charAt(j);
                             //lenght() 
                             // Precondition: The calling code passes no parameter
                             // Postcondition: establishes a connection with the opened file
                             //charAt()
                             // Precondition: The calling code provides value type int 
                             // Postcondition: assigns char value to variable
                             
                             // Test conditions
                             if(Character.isWhitespace(c)){
                             // Precondition: The calling code provides char value
                             // Postcondition: return true if condition met or false if not
                                 wc++;
                             }
                             if(c=='.'){
                                 pp++;
                             }
                             if(c=='?'){
                                 qq++;
                             }
                             if(c=='!'){
                                 ex++;
                             }
                             if(c==','){
                                 cc++;
                             }
                             if(c==':'){
                                 cl++;
                             }
                             if(c==';'){
                                 scl++;
                             }
                             if(c=='\''){
                                 sq++;
                             }
                             if(c=='\"'){
                                 dq++;
                             }
                             if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||c=='O'||c=='U'){
                                 vv++;
                             }
                             if(Character.isLetterOrDigit(c)){
                             // Precondition: The calling code provides char value
                             // Postcondition: return true if condition met or false if not
                                 an++;
                             }
                             
                        }
                        line = inputFile.readLine();
                        // Precondition: The calling code passed no parameter 
                        // Postcondition: assigns string to variable
		}
                
                // Methods that display results on screen and print to output file
                // Precondition: The calling code provides a string 
                // Postcondition: displays string on screen
                System.out.println("Amount of words in the file: " + wc);
                outputFile.println("Amount of words in the file: " + wc);
                System.out.println("Amount of lines in the file: " + lc);
                outputFile.println("Amount of lines in the file: " + lc);
                System.out.println("Amount of alphanumeric characters in the file: " + an);
                outputFile.println("Amount of alphanumeric characters in the file: " + an);
                System.out.println("Number of sentences in the file: " + (pp+ex+qq));
                outputFile.println("Number of sentences in the file: " + (pp+ex+qq));
                System.out.println("Amount of vowels in the file: " + vv);
                outputFile.println("Amount of vowels in the file: " + vv);
                System.out.println("Amount of punctuations in the file: " + (pp+ex+qq+cc+cl+scl+dq+sq));
                outputFile.println("Amount of punctuations in the file: " + (pp+ex+qq+cc+cl+scl+dq+sq));
                
                // Output and input file are closd
                // Precondition: The calling code passes no parameter 
                // Postcondition: close file
                outputFile.close();
		inputFile.close();
    }    
}
