/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarytree;

import java.util.*;

public class BinaryTree {
    
    private String infix;//Variable that holds user input
    private String postfix;//Variable that holds converted user input
    //String of character for checking input
    private final String VALID_SYMBOLS = "()1234567890/*%+-\t ";
    //String of character for checking input
    private final String VALID_NUMBERS = "1234567890";
    //String of character for checking input
    private final String OPS = "/*%+-()";
    //Declaration of objects
    private Stack<Character> opstack;
    private StringBuilder postfixBuilder;
    private Stack<Character> parenthesis;
    private Stack<Tree<String>> binstack;

    public static void main(String[] args) {
        //Creates scanner object
        Scanner keyboard = new Scanner(System.in);
        // Prints out message to user
        System.out.println("Enter infix expression:");
        String input = keyboard.nextLine();
        // Precondition: The calling code passes no parameter 
        // Postcondition: assigns string to variable
        //Creates BinaryTree object
        BinaryTree calc = new BinaryTree(input);
        //method call, checks for illegal expression
        calc.ckexp();
        //method call, converts user infix to postfix
        calc.doPostfix();
        // Prints out message to user
        System.out.println("Converted expresion:" + calc.getPostfix());
        //Creates BinaryTree object
        Tree eTree = calc.buildTree();
        // Prints out message to user
        System.out.println("Answer to expression: " + calc.evalTree(eTree));
        // Prints out a blank line
        System.out.println();
    }

    //construct that initializes variables and objects
    public BinaryTree(String exp) {
        infix = exp;
        postfix = "";
        binstack = new Stack<Tree<String>>();
        opstack = new Stack<Character>();
        parenthesis = new Stack<Character>();
    }

    //method that returns the postfix conversion
    public String getPostfix() {
        return postfix;
    }

    //method that checks for illegal expression
    public void ckexp() {
        StringBuilder buffer = new StringBuilder();
        infix = infix.replaceAll(" ", "");
        boolean error = false;

        for (int i = 0; i < infix.length(); i++) {
            char token = infix.charAt(i);
            char nexttoken = 0;
            boolean lastchar = false;
            boolean isTokenDigit = false;
            boolean isNextTokenDigit = false;;

            if (i == infix.length() - 1) {
                lastchar = true;
            } else {
                nexttoken = infix.charAt(i + 1);
            }

            if (VALID_SYMBOLS.indexOf(token) < 0) {
                System.out.println("Error in expression.");
                error = true;
                break;
            }

            if (!lastchar) {
                isTokenDigit = Character.isDigit(token);
                isNextTokenDigit = Character.isDigit(nexttoken);
                if ((isTokenDigit && nexttoken == '(')
                        || (token == ')' && isNextTokenDigit)
                        || (token == ')') && (nexttoken == '(')
                        || (token == ')' && nexttoken == '^')) {
                    buffer.append(token + "*" + nexttoken);
                    i++;

                } else {
                    buffer.append(token);
                }
            } else if (lastchar) {
                buffer.append(token);
            }
        }
        if (error) {
            infix = postfix = "NA";
        } else {
            infix = buffer.toString();
        }
    }

    //method that sets precedence
    private int precedence(char token) {
        int result = 0;
        if (token == '*' || token == '/' || token == '%') {
            result = 2;
        } else if (token == '+' || token == '-') {
            result = 1;
        }

        return result;
    }

    //method that processes operators
    private void gotOperator(char op) {
        if (opstack.empty() || op == '(') {
            opstack.push(op);
        } else {
            char topop = opstack.peek();
            if (precedence(op) > precedence(topop)) {
                opstack.push(op);
            } else {
                while (!opstack.empty() && precedence(op) <= precedence(topop)) {
                    opstack.pop();
                    if (topop == '(') {
                        break;
                    }
                    postfixBuilder.append(' ');
                    postfixBuilder.append(topop);
                    if (!opstack.empty()) {
                        topop = opstack.peek();
                    }
                }
                if (op != ')') {
                    opstack.push(op);
                }
            }
        }
    }

    //method that check if token is operator
    private boolean isOp(char op) {
        return OPS.indexOf(op) != -1;
    }

    //methof that converts infix input to postfix
    public void doPostfix() {
        boolean error = false;

        if (!infix.equals("NA")) {
            postfixBuilder = new StringBuilder();

            for (int i = 0; i < infix.length(); i++) {
                char token = infix.charAt(i);

                try {
                    if (token == '(') {
                        parenthesis.push(token);
                    }
                    if (token == ')') {
                        parenthesis.pop();
                    }
                } catch (EmptyStackException e) {
                    System.out.println("Missing parenthesis.");
                    postfix = "NA";
                    error = true;
                    break;
                }

                if (VALID_NUMBERS.indexOf(token) >= 0) {
                    postfixBuilder.append(token);
                } else if (isOp(token)) {
                    gotOperator(token);
                    postfixBuilder.append(' ');
                }

            }
            if (!parenthesis.empty()) {
                System.out.println("Missing parenthesis.");
                postfix = "NA";
                error = true;

            }
            if (!error) {
                while (!opstack.empty()) {
                    char op = opstack.pop();
                    if (op == '(') {
                    }
                    postfixBuilder.append(' ');
                    postfixBuilder.append(op);

                }

                postfix = postfixBuilder.toString().trim().replaceAll("  ", " ").replaceAll("   ", " ");
            }
        }
    }

    //method that builds a tree
    public Tree<String> buildTree() {
        if (!postfix.equals("NA")) {
            String[] elements = postfix.split(" ");

            for (String token : elements) {
                if (token.isEmpty()) {
                } else if (Character.isDigit(token.charAt(0))) {
                    Tree<String> element = new Tree<String>(token, null, null);
                    binstack.push(element);
                } else {
                    Tree<String> right = binstack.pop();
                    Tree<String> left = binstack.pop();
                    Tree<String> element = new Tree<String>(token, left, right);
                    binstack.push(element);
                }
            }
            return binstack.pop();

        } else {
            return null;
        }

    }

    //method that parses the data from the binary tree
    public double evalTree(Tree<String> eTree) {
        if (eTree == null) {
            return 0;
        } else if (eTree.isLeaf()) {
            return Double.parseDouble(eTree.root.data);
        } else {
            char op = eTree.root.data.charAt(0);

            double left = evalTree(eTree.getLeftSubtree());
            double right = evalTree(eTree.getRightSubtree());


            return evaluate(op, left, right);
        }
    }

    //method that calculates the answer of the expression
    private double evaluate(char op, double left, double right) {
        double result = 0;

        switch (op) {
            case '+':
                result = left + right;
                break;
            case '-':
                result = left - right;
                break;
            case '*':
                result = left * right;
                break;
            case '/':
                result = left / right;
                break;
        }
        return result;
    }
}
